verticesCube = [
    -1,  1,  1, -1, -1,  1, 1, -1;
    -1, -1,  1,  1, -1, -1, 1,  1;
    -1, -1, -1, -1,  1,  1, 1,  1;
     1,  1,  1,  1,  1,  1, 1,  1
    ];

verticesCube1 = [
    1, 0, 0, 1;
    0, 1, 0, 2;
    0, 0, 1, 3;
    0, 0, 0, 1
    ]*verticesCube;

verticesCube2 = [
    1, 0, 0, 4;
    0, 1, 0, 3;
    0, 0, 1, 4;
    0, 0, 0, 1
    ]*verticesCube;

facesCube = [
    1, 2, 6, 5;
    2, 3, 7, 6;
    3, 4, 8, 7;
    4, 1, 5, 8;
    1, 2, 3, 4;
    5, 6, 7, 8
    ];

verticesPyramid = [
    -1, -1, 1,  1, 0;
    -1,  1, 1, -1, 0;
     0,  0, 0,  0, 1;
     1,  1, 1,  1, 1
    ];

facesPyramid = [
    1, 2, 5;
    2, 3, 5;
    3, 4, 5;
    4, 1, 5;
    ];

Xs = 2;
Ys=3;
Zs=0.5;

Sc=[
    Xs, 0, 0, 0;
    0, Ys, 0, 0;
    0, 0, Zs, 0;
    0, 0, 0, 1
    ];

Tx = 1;
Ty = 2;
Tz = 3;

Ts=[
    1, 0, 0, Tx;
    0, 1, 0, Ty;
    0, 0, 1, Tz;
    0, 0, 0, 1
    ];
al=pi/3;
bt=pi/4;
gm=pi/7;
Rtx =[1, 0, 0, 0;
      0, cos(al), -sin(al), 0;
      0, sin(al), cos(al), 0;
      0, 0, 0, 1
    ];

Rty =[cos(bt), 0, sin(bt), 0;
      0, 1, 0, 0;
      -sin(bt), 0, cos(bt), 0;
      0, 0, 0, 1
    ];

Rtz = [cos(gm), -sin(gm), 0, 0;
       sin(gm), cos(gm), 0, 0;
       0, 0, 1, 0;
       0, 0, 0, 1
    ];

R1 = Rtx*Rty*Rtz;
R2 = Rtx*Rtz*Rty;
R3 = Rty*Rtx*Rtz;
R4 = Rty*Rtz*Rtx;
R5 = Rtz*Rtx*Rty;
R6 = Rtz*Rty*Rtx;

%R= [R1; R2; R3; R4; R5; R6];
R=zeros(4,4,6);
R(:,:,1) = R1;
R(:,:,2) = R2;
R(:,:,3) = R3;
R(:,:,4) = R4;
R(:,:,5) = R5;
R(:,:,6) = R6;

%figure(1);
%DrawShape(verticesCube, facesCube, 'blue');
%DrawShape(Sc*verticesCube, facesCube, 'red');
%DrawShape(Ts*Sc*verticesCube, facesCube, 'black');
%{
for i=1:6
    figure(i)
    DrawShape(Ts*Sc*verticesCube, facesCube, 'black');
    hold on
    DrawShape(Ts*R(:,:,i)*Sc*verticesCube, facesCube, 'red');
    legend('original cube', 'rotated');
    hold off
end
%}

Tz=[
    1, 0, 0, 1;
    0, 1, 0, 1;
    0, 0, 1, 0;
    0, 0, 0, 1
    ];

figure(1);
DrawShape(verticesCube, facesCube, 'black');
DrawShape(inv(Tz)*Rtz*Tz*verticesCube, facesCube, 'red');
legend('original cube', 'rotated');


%{
figure(2)
DrawShape(verticesCube, facesCube, 'blue');
hold on
DrawShape(Ts*verticesCube, facesCube, 'red');
legend('original cube', 'translated');
hold off
%}


%{
figure(2);
DrawShape(verticesPyramid, facesPyramid, 'blue');
DrawShape(verticesPyramid, [1, 2, 3, 4], 'blue');
%}
%{

figure(1);
a=pi/4;
b=-pi/4;
c=0;
x=-1.5; y=-1; z=5;

V=[-0.1,0.1,-0.1,0.1,0.5,100];
DrawShape(perMat(V(1),V(2),V(3),V(4),V(5),V(6))*camMat(x,y,z,a,b,c)*verticesCube, facesCube, 'red');
DrawShape(perMat(V(1),V(2),V(3),V(4),V(5),V(6))*camMat(x,y,z,a,b,c)*verticesCube1, facesCube, 'green');
DrawShape(perMat(V(1),V(2),V(3),V(4),V(5),V(6))*camMat(x,y,z,a,b,c)*verticesCube2, facesCube, 'blue');
view([0 90]);
xlabel('x');
ylabel('y');    
zlabel('z');
xlim([-5 5]);
ylim([-5 5]);
%}

function M = perMat(l,r,b,t,n,f)
    M=[
      2*n/(r-l), 0,          (r+l)/(r-l),  0;
      0,         2*n/(t-b),  (t+b)/(t-b),  0;
      0,         0,         -(f+n)/(f-n), -2*f*n/(f-n);
      0,         0,         -1,             0
    ];
    disp(M)
end

function M = camMat(x,y,z,a,b,c)
    %a, b, c - cam's yaw, pitch, roll
    %x, y, z - cam's x, y, z coordinates
    
    Rtx =[1, 0, 0, 0;
      0, cos(b), -sin(b), 0;
      0, sin(b), cos(b), 0;
      0, 0, 0, 1
    ];

    Rty =[cos(c), 0, sin(c), 0;
      0, 1, 0, 0;
      -sin(c), 0, cos(c), 0;
      0, 0, 0, 1
    ];

    Rtz = [cos(a), -sin(a), 0, 0;
       sin(a), cos(a), 0, 0;
       0, 0, 1, 0;
       0, 0, 0, 1
    ];
    
    F = Rty*Rtx*Rtz*[1;0;0;0];
    U = Rty*Rtx*Rtz*[0;0;1;0];
    R=[cross(F(1:3),U(1:3));0];

    X=[x;y;z;1];
    M=horzcat(F,U,R,X);
end

function DrawShape ( vertices , faces , color )
    %patch('Vertices', ( vertices (1:3 ,:) ./ vertices (4 ,:) )' , 'Faces', faces ,  'FaceColor', color );
    patch('Vertices', ( vertices (1:3 ,:) ./ vertices (4 ,:) )' , ...
        'Faces', faces ,  'FaceColor', color, ...
        'EdgeColor', 'black','FaceAlpha',.1);
    axis equal;
    view(3);
    fontsize(gca,24,"points")
    x0=100;
    y0=100;
    width=800;
    height=500;
    set(gcf,'position',[x0,y0,width,height])
end
